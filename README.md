<br />
<p align="center">
  <a href="https://gitlab.com/mitchellfranklin/devops-email-notification-remover-chrominium">
    <img src="src/icon128.png" alt="Logo" width="80" height="80">
  </a>

  <h3 align="center">Boomi Platform Enhancer Extension</h3>

  <p align="center">
    Provides User Enhancements to the Boomi Platform
    <br />
    <br />
    <a href="https://#">View Chrome Store</a>
    ·
    <a href="https://gitlab.com/mitchellfranklin/devops-email-notification-remover-chrominium/-/issues">Request a feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
## Table of Contents

- [Table of Contents](#table-of-contents)
- [About The Project](#about-the-project)
  - [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Installation](#installation)
- [Usage](#usage)
- [Roadmap](#roadmap)
- [Contributing](#contributing)
- [License](#license)
- [Contact](#contact)
- [Acknowledgements](#acknowledgements)



<!-- ABOUT THE PROJECT -->
## About The Project

The DevOps Email Notification Remover allows for the copy and paste of email information into Dev Ops cards allows the automatic removal of email address i.e. myname@gmail.com so that when adding comments or data to the card it does not auto create a notification.

when an email address is added to a card it automatically uses this as a notification to email address, 99% if times you are wanting to only provide information (Note the @ notification still works as required) 



### Built With
The main frameworks of this extension are the following + the ide used.
* [JQuery](https://jquery.com)
* [JavaScript](https://www.javascript.com/)
* [Visual Studio Code](https://code.visualstudio.com/)



## Getting Started

To get started simply visit the [Chrome WebStore](https://chrome.google.com/webstore/") and install the addon; once done it will auto-enable when on the DevOps Web Platform.


### Installation

1. [Visit the Chrome WebStore](https://chrome.google.com/webstore/")
2. Click Install



## Usage

This extension autoenables upon focus or unfocus of the text


  ![](chromewebstore/image1.png)
  




<!-- ROADMAP -->
## Roadmap

See the [open Requests / Changes](https://gitlab.com/mitchellfranklin/devops-email-notification-remover-chrominium/-/issues) for a list of proposed features (and known issues).



## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request




## License

Distributed under the MIT License. See `LICENSE` for more information.




## Contact

Name - [Mitchell Franklin](https://mitchellfranklin.info) - mitchellfranklin@gmail.com





<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [Choose an Open Source License](https://choosealicense.com)
* [Visual Studio Code](https://code.visualstudio.com/)
