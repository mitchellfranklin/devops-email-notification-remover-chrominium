    const global_listners = setInterval(() => {

        const listenerClass = (selector, callback) => {
            let elements = document.querySelectorAll(`${selector}:not(.bph-load-done)`);
            if (elements.length) {
                [...elements].forEach(el => {
                    el.classList.add('bph-load-done');
                    if (Array.isArray(callback)) {
                        callback.forEach(cb => {
                            cb(el)
                        })
                    } else {
                        callback(el);
                    }
                })
            }
        }

        listenerClass('.work-item-form', add_textwidget_listener);

    }, 1000)



